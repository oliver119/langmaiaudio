package com.langmai.audio;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by User on 6/20/2017.
 */

public class Session {
    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void set_search_key(String search_key) {
        prefs.edit().putString("search_key", search_key).commit();

    }

    public String get_search_key() {
        String search_key = prefs.getString("search_key","");
        return search_key;
    }

    public void set_ver_DB(String ver_DB) {
        prefs.edit().putString("ver_DB", ver_DB).commit();

    }

    public String get_ver_DB() {
        String ver_DB = prefs.getString("ver_DB", "");
        return ver_DB;
    }





}
