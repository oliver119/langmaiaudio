package com.langmai.audio.RealmDB;

import android.content.Context;
import android.support.v4.media.MediaMetadataCompat;

import com.langmai.audio.RealmDB.Model.CollectionRealm;
import com.langmai.audio.RealmDB.Model.SongRealm;
import com.langmai.audio.model.RemoteJSONSource;
import com.langmai.audio.utils.MediaIDHelper;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;


/**
 * Created by User on 8/14/2017.
 */

public class RealmController {
    Realm realm;
    Context view;

    public RealmController(Context context) {

        this.view = context;
        Realm.init(view);
        RealmConfiguration config = new RealmConfiguration.Builder().schemaVersion(1).deleteRealmIfMigrationNeeded().build();


        Realm.setDefaultConfiguration(config);


        realm = Realm.getDefaultInstance();

    }

    public void deleteall() {
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.deleteAll();
            }
        });

    }

    public void add_song(String name, String icon, String album, String artist, String genre, int duration, String type, String id, String Source) {

        realm.beginTransaction();

        final SongRealm Song = new SongRealm();
        Song.setSong_id(id);
        Song.setAllbum(album);
        Song.setArtist(artist);
        Song.setGenre(genre);
        Song.setDuration(duration);
        Song.setName(name);
        Song.setIcon(icon);
        Song.setType(type);
        Song.setSource(Source);


        // insert data

        realm.copyToRealmOrUpdate(Song);

        realm.commitTransaction();


    }

    public void add_collection(String name, String id, String image, String type, String time) {


        //realm.beginTransaction();

        final CollectionRealm collection = new CollectionRealm();

        collection.setName(name);
        collection.setId(id);
        collection.setImage(image);
        collection.setType(type);
        collection.setTime(time);


        //  realm.commitTransaction();

        realm.executeTransactionAsync(new Realm.Transaction() {
            public void execute(Realm bgRealm) {
                bgRealm.copyToRealmOrUpdate(collection);

            }
        });

    }


    public ArrayList<MediaMetadataCompat> Tophit_US() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<SongRealm> query = realm.where(SongRealm.class);

        query.equalTo("type", MediaIDHelper.MEDIA_ID_MUSICS_BY_TOPHIT_US);

        RealmResults<SongRealm> results = query.findAll();
        results.sort("name");

        for (SongRealm song : results) {

            track.add(RemoteJSONSource.buildFromCursor(song.getName(), song.getAllbum(), song.getArtist(), song.getGenre(), song.getSource(), song.getIcon(), 3, 10, song.getDuration(), song.getSong_id()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> Tophit_VPop() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<SongRealm> query = realm.where(SongRealm.class);

        query.equalTo("type", MediaIDHelper.MEDIA_ID_MUSICS_BY_TOPHIT_VPOP);

        RealmResults<SongRealm> results = query.findAll();

        for (SongRealm song : results) {

            track.add(RemoteJSONSource.buildFromCursor(song.getName(), song.getAllbum(), song.getArtist(), song.getGenre(), song.getSource(), song.getIcon(), 3, 10, song.getDuration(), song.getSong_id()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> Sam_Nguyen() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<SongRealm> query = realm.where(SongRealm.class);

        query.equalTo("type", MediaIDHelper.SAM_NGUYEN);

        RealmResults<SongRealm> results = query.findAll();

        for (SongRealm song : results) {

            track.add(RemoteJSONSource.buildFromCursor(song.getName(), song.getAllbum(), song.getArtist(), song.getGenre(), song.getSource(), song.getIcon(), 3, 10, song.getDuration(), song.getSong_id()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> Niem_But() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<SongRealm> query = realm.where(SongRealm.class);

        query.equalTo("type", MediaIDHelper.NIEM_BUT);

        RealmResults<SongRealm> results = query.findAll();

        for (SongRealm song : results) {

            track.add(RemoteJSONSource.buildFromCursor(song.getName(), song.getAllbum(), song.getArtist(), song.getGenre(), song.getSource(), song.getIcon(), 3, 10, song.getDuration(), song.getSong_id()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> Kinh_Van() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<SongRealm> query = realm.where(SongRealm.class);

        query.equalTo("type", MediaIDHelper.KINH_VAN);

        RealmResults<SongRealm> results = query.findAll();

        for (SongRealm song : results) {

            track.add(RemoteJSONSource.buildFromCursor(song.getName(), song.getAllbum(), song.getArtist(), song.getGenre(), song.getSource(), song.getIcon(), 3, 10, song.getDuration(), song.getSong_id()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> Tophit_KPop() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<SongRealm> query = realm.where(SongRealm.class);

        query.equalTo("type", MediaIDHelper.MEDIA_ID_MUSICS_BY_TOPHIT_KPOP);

        RealmResults<SongRealm> results = query.findAll();

        for (SongRealm song : results) {

            track.add(RemoteJSONSource.buildFromCursor(song.getName(), song.getAllbum(), song.getArtist(), song.getGenre(), song.getSource(), song.getIcon(), 3, 10, song.getDuration(), song.getSong_id()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> CollectionMenu() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<CollectionRealm> query = realm.where(CollectionRealm.class);
        query.equalTo("type", MediaIDHelper.COLLECTION_MENU);


        RealmResults<CollectionRealm> results = query.findAll();
        results.sort("name");

        for (CollectionRealm collect : results) {

            track.add(RemoteJSONSource.buildFromCursor(collect.getName(), collect.getTime(), collect.getTime(), collect.getId(), collect.getId(), collect.getImage(), 3, 10, 11, collect.getId()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> ThienCa_CD() {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<CollectionRealm> query = realm.where(CollectionRealm.class);
        query.equalTo("type", MediaIDHelper.THIEN_CA);


        RealmResults<CollectionRealm> results = query.findAll();
        results.sort("name");

        for (CollectionRealm collect : results) {

            track.add(RemoteJSONSource.buildFromCursor(collect.getName(), "unkonw", "unkonw", collect.getId(), "unkonw", collect.getImage(), 3, 10, 11, collect.getId()));

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> CollectionMenuCon(String idcha) {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<CollectionRealm> query = realm.where(CollectionRealm.class);
        query.equalTo("type", MediaIDHelper.COLLECTION_MENU_CON);


        RealmResults<CollectionRealm> results = query.findAll();
        results.sort("name");

        for (CollectionRealm collect : results) {

            String[] chaid = collect.getId().split("_");
            if (chaid[0].equals(idcha)) {
                track.add(RemoteJSONSource.buildFromCursor(collect.getName(), "unkonw", "unkonw", collect.getId(), "unkonw", collect.getImage(), 3, 10, 11, collect.getId()));

            }

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> CollectionMenuTopic(String idTopic) {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<CollectionRealm> query = realm.where(CollectionRealm.class);
        query.equalTo("type", MediaIDHelper.COLLECTION_MENU_TOPIC);


        RealmResults<CollectionRealm> results = query.findAll();
        // results.sort("name");

        for (CollectionRealm collect : results) {

            String[] chaid = collect.getId().split("_");
            if ((chaid[0] + "_" + chaid[1]).equals(idTopic)) {
                track.add(RemoteJSONSource.buildFromCursor(collect.getName(), "unkonw", "unkonw", collect.getId(), "unkonw", collect.getImage(), 3, 10, 11, collect.getId()));

            }

        }

        return track;

    }

    public ArrayList<MediaMetadataCompat> getsongCollection(String idTopic) {


        ArrayList<MediaMetadataCompat> track = new ArrayList<MediaMetadataCompat>();
        RealmQuery<SongRealm> query = realm.where(SongRealm.class);

        query.equalTo("type", idTopic);

        RealmResults<SongRealm> results = query.findAll();
        //results.sort("name");

        for (SongRealm song : results) {

            track.add(RemoteJSONSource.buildFromCursor(song.getName(), song.getAllbum(), song.getArtist(), song.getGenre(), song.getSource(), song.getIcon(), 3, 10, song.getDuration(), song.getSong_id()));

        }

        return track;

    }

}
