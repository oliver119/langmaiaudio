package com.langmai.audio.Enity;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 6/16/2017.
 */
@IgnoreExtraProperties
public class User_Enity {
    private String UseriD;
    private String email;
    private String Country;
    private String Avatar_Url;


    public String getAvatar_Url() {
        return Avatar_Url;
    }


    public void setAvatar_Url(String avatar_Url) {

        Avatar_Url = avatar_Url;
    }


    private String Name;
    private String Phone;


    public User_Enity(String useriD, String email, String country, String name, String phone, String user_Type, String avatar_Url) {
        UseriD = useriD;
        this.email = email;
        Country = country;

        Name = name;
        Phone = phone;
        User_Type = user_Type;
        Avatar_Url = avatar_Url;

    }


    private String User_Type;


    public User_Enity() {

    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        //  result.put("uid", UseriD);
        result.put("email", email);
        result.put("country", Country);
        result.put("name", Name);
        result.put("phone", Phone);
        result.put("user_Type", User_Type);
        result.put("avatar", Avatar_Url);


        return result;
    }


    public String getUseriD() {
        return UseriD;
    }

    public void setUseriD(String useriD) {
        UseriD = useriD;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }


    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getUser_Type() {
        return User_Type;
    }

    public void setUser_Type(String user_Type) {
        User_Type = user_Type;
    }
}
