package com.langmai.audio.api;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestfulClient {
    private static APIService ideaService;
    private static String baseUrl = "http://api.mp3.zing.vn/";
    private static Retrofit INSTANCE;
    private static Gson gson;
    public static Retrofit getInstance() {
        if (INSTANCE == null) {
            setupRestClient();
        }
        return INSTANCE;
    }


    public static void setupRestClient() {
//        addHeadersRequiredForAllRequests(httpClient, BuildConfig.VERSION_NAME);

        gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Response response = chain.proceed(chain.request());
                                return response;
                            }
                        })
                .build();

        INSTANCE = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static APIService getClient() {
        if (ideaService == null) {
            ideaService = getInstance().create(APIService.class);
        }
        return ideaService;
    }

}
