package com.langmai.audio.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 8/16/2017.
 */

public class RestfulClientCollection {
    private static APIService ideaService;
    //   private static String baseUrl = "https://radio.zing.vn/xml/radio/";
    private static Retrofit INSTANCE;
    private static Gson gson;

    public static Retrofit getInstance(String url) {

        setupRestClient(url);

        return INSTANCE;
    }


    public static void setupRestClient(String url) {
//        addHeadersRequiredForAllRequests(httpClient, BuildConfig.VERSION_NAME);

        gson = new GsonBuilder()
                .setLenient()
                .create();

        OkHttpClient okClient = new OkHttpClient.Builder()
                .addInterceptor(
                        new Interceptor() {
                            @Override
                            public Response intercept(Chain chain) throws IOException {
                                Response response = chain.proceed(chain.request());
                                return response;
                            }
                        })
                .build();

        INSTANCE = new Retrofit.Builder()
                .baseUrl(url)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public static APIService getClient(String url) {

        ideaService = getInstance(url).create(APIService.class);

        return ideaService;
    }

}
