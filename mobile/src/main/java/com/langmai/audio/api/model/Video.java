package com.langmai.audio.api.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Video {

    @SerializedName("video_id")
    @Expose
    private Integer videoId;
    @SerializedName("song_id_encode")
    @Expose
    private String songIdEncode;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("artist")
    @Expose
    private String artist;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("download_status")
    @Expose
    private Integer downloadStatus;
    @SerializedName("copyright")
    @Expose
    private Integer copyright;
    @SerializedName("co_id")
    @Expose
    private Integer coId;
    @SerializedName("ad_status")
    @Expose
    private Integer adStatus;
    @SerializedName("license_status")
    @Expose
    private Integer licenseStatus;

    public Integer getVideoId() {
        return videoId;
    }

    public void setVideoId(Integer videoId) {
        this.videoId = videoId;
    }

    public String getSongIdEncode() {
        return songIdEncode;
    }

    public void setSongIdEncode(String songIdEncode) {
        this.songIdEncode = songIdEncode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDownloadStatus() {
        return downloadStatus;
    }

    public void setDownloadStatus(Integer downloadStatus) {
        this.downloadStatus = downloadStatus;
    }

    public Integer getCopyright() {
        return copyright;
    }

    public void setCopyright(Integer copyright) {
        this.copyright = copyright;
    }

    public Integer getCoId() {
        return coId;
    }

    public void setCoId(Integer coId) {
        this.coId = coId;
    }

    public Integer getAdStatus() {
        return adStatus;
    }

    public void setAdStatus(Integer adStatus) {
        this.adStatus = adStatus;
    }

    public Integer getLicenseStatus() {
        return licenseStatus;
    }

    public void setLicenseStatus(Integer licenseStatus) {
        this.licenseStatus = licenseStatus;
    }

}