package com.langmai.audio.api;


import com.langmai.audio.api.model.Collection_List_Song;
import com.langmai.audio.api.model.ZingSong;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface APIService {


    @GET("api/mobile/song/getsonginfo")
    Call<ZingSong> get_ZingSong(
            @Query("requestdata") String data
    );


    @GET("?format=json")
    Call<Collection_List_Song> get_SongCollectionID(

    );

}


