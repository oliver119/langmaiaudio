package com.langmai.audio;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

/**
 * Created by User on 8/31/2017.
 */

public class MyApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
}