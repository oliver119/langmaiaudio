/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.langmai.audio.model;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.media.MediaMetadataCompat;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.langmai.audio.CreateData_Callback;
import com.langmai.audio.RealmDB.RealmController;
import com.langmai.audio.ui.PhapThoaiActivity;
import com.langmai.audio.utils.DispatchGroup;
import com.langmai.audio.utils.LogHelper;
import com.langmai.audio.utils.MediaIDHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Utility class to get a list of MusicTrack's based on a server-side JSON
 * configuration.
 */
public class RemoteJSONSource implements MusicProviderSource {


    private static final String TAG = LogHelper.makeLogTag(RemoteJSONSource.class);
    private static final String JSON_MUSIC = "Value";
    private static final String JSON_TITLE = "title";
    //  public static final ArrayList<MediaMetadataCompat> Tracktophit = new ArrayList<MediaMetadataCompat>();
    private static final String JSON_ALBUM = "genre";
    private static final String JSON_ARTIST = "genre";
    private static final String JSON_GENRE = "genre";
    private static final String JSON_SOURCE = "stream_url";
    private static final String JSON_IMAGE = "artwork_url";
    private static final String JSON_TRACK_NUMBER = "download_count";
    private static final String JSON_TOTAL_TRACK_COUNT = "likes_count";
    private static final String JSON_DURATION = "duration";
    public static String SEARCH =
            "";
    public static String CATALOG_URL =
            "https://api.soundcloud.com/tracks?client_id=a3e059563d7fd3372b49b37f00a00bcf&q=\"jay chou\"&limit=200";
    static JSONArray jarray = null;
    static JSONObject Object = null;
    public CreateData_Callback onComplete;
    RealmController realmController;
    private Runnable runnable;
    private DispatchGroup group = new DispatchGroup();


    public  RemoteJSONSource(String url, String search){

        if(search.equals("1")){
            CATALOG_URL= "https://api.soundcloud.com/tracks?client_id=a3e059563d7fd3372b49b37f00a00bcf&q=\"jay chou\"&limit=200";


        }else {
            CATALOG_URL = url;
        }

    }

    public RemoteJSONSource() {

    }


    public static MediaMetadataCompat buildFromJSON(JSONObject json, String basePath) throws JSONException {
        String title = json.getString(JSON_TITLE);
        String album = json.getString(JSON_ALBUM);
        String artist = json.getString(JSON_ARTIST);
        // String genre = json.getString(JSON_GENRE);

        String genre = "unknow";

        String source = json.getString(JSON_SOURCE);

        String iconUrl = json.getString(JSON_IMAGE).replace("large", "t500x500");


        int trackNumber = json.getInt(JSON_TRACK_NUMBER);
        int totalTrackCount = json.getInt(JSON_TOTAL_TRACK_COUNT);
        int duration = json.getInt(JSON_DURATION); // ms

        LogHelper.d(TAG, "Found music track: ", json);

        // Media is stored relative to JSON file
        if (source.startsWith("http")) {
            source = basePath + source + "?client_id=a3e059563d7fd3372b49b37f00a00bcf";
        }
        if (iconUrl.startsWith("http")) {
            iconUrl = basePath + iconUrl;
        }
        // Since we don't have a unique ID in the server, we fake one using the hashcode of
        // the music source. In a real world app, this could come from the server.
        String id = String.valueOf(source.hashCode());

        // Adding the music source to the MediaMetadata (and consequently using it in the
        // mediaSession.setMetadata) is not a good idea for a real world music app, because
        // the session metadata can be accessed by notification listeners. This is done in this
        // sample for convenience only.
        //noinspection ResourceType
        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MusicProviderSource.CUSTOM_METADATA_TRACK_SOURCE, source)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE, genre)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, trackNumber)
                .putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS, totalTrackCount)
                .build();
    }

    public static MediaMetadataCompat buildFromCursor(String title, String album, String artist, String genre, String source, String iconUrl, int trackNumber, int totalTrackCount, int duration, String id) {

        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MusicProviderSource.CUSTOM_METADATA_TRACK_SOURCE, source)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putLong(MediaMetadataCompat.METADATA_KEY_DURATION, duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE, genre)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .putLong(MediaMetadataCompat.METADATA_KEY_TRACK_NUMBER, trackNumber)
                .putLong(MediaMetadataCompat.METADATA_KEY_NUM_TRACKS, totalTrackCount)
                .build();
    }

    public static Uri getAlbumArtUri(long albumId) {
        return ContentUris.withAppendedId(Uri.parse("content://media/external/audio/albumart"), albumId);
    }

    @Override
    public Iterator<MediaMetadataCompat> iterator() {

        try {
            int slashPos = CATALOG_URL.lastIndexOf('/');
           // String path = CATALOG_URL.substring(0, slashPos + 1);
            String path = "";
            //JSONObject jsonObj = fetchJSONFromUrl(CATALOG_URL);



            ArrayList<MediaMetadataCompat> tracks = new ArrayList<>();

          //  if (jsonObj != null) {
            //    JSONArray jsonTracks = jsonObj.getJSONArray(JSON_MUSIC);

            JSONArray jsonTracks =fetchJSONFromUrlArray(CATALOG_URL);

                if (jsonTracks != null) {
                    for (int j = 0; j < jsonTracks.length(); j++) {
                        tracks.add(buildFromJSON(jsonTracks.getJSONObject(j), path));
                    }
                }
       //     }
            return tracks.iterator();
        } catch (JSONException e) {
            LogHelper.e(TAG, e, "Could not retrieve music list");
            throw new RuntimeException("Could not retrieve music list", e);
        }
    }

    @Override
    public Iterator<MediaMetadataCompat> Library_iterator(final Context context) {
        ContentResolver musicResolver = context.getContentResolver();
        // Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;


        //  Uri musicUri = MediaStore.Audio.Media.getContentUriForPath(Environment.getExternalStorageDirectory().getPath() + "/LangMaiAudio");
        //  Uri musicUri = Uri.parse(Environment.getExternalStorageDirectory().getPath() + "/LangMaiAudio");


        ArrayList<MediaMetadataCompat> tracks = new ArrayList<>();
        try {


            Cursor musicCursor = musicResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    null,
                    MediaStore.Audio.Media.DATA + " like ? ",
                    new String[]{"%LangMaiAudio%"},
                    null);
            int count = 0;

            if (musicCursor != null) {
                count = musicCursor.getCount();

                if (count > 0)
        {
            while (musicCursor.moveToNext())
            {
                String source = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                String title = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.TITLE));
                String album = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                String artist = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST));
                String genre = "unknow";
                String iconUrl = getAlbumArtUri(Long.parseLong(musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)))).toString();


                int Duration = Integer.parseInt(musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media.DURATION)));
                String id = musicCursor.getString(musicCursor.getColumnIndex(MediaStore.Audio.Media._ID));

                tracks.add(buildFromCursor(title, album, artist, genre, source, iconUrl, 3, 3, Duration, id));
            }

        }
            }

            musicCursor.close();

        } catch (NullPointerException e) {


        }


        return tracks.iterator();

    }

    @Override
    public void creatingdata(final Context context, final KProgressHUD hud) {


        realmController = new RealmController(context);
        realmController.deleteall();


        runnable = new Runnable() {
            @Override
            public void run() {
                hud.dismiss();

                onComplete = new PhapThoaiActivity();
                onComplete.callback(context);
                //Toast.makeText(context, "oK", Toast.LENGTH_SHORT).show();
            }
        };

        group.enter();
        group.notify(runnable);
        addCollection(context);
        load_niembut(realmController);

        add_thienca(context);

        load_KinhNhac(context);

        load_NgheKinh(context);


    }


    /////

    /**
     * Download a JSON file from a server, parse the content and return the JSON
     * object.
     *
     * @return result JSONObject containing the parsed representation.
     */


    private JSONArray fetchJSONFromUrlArray(String urlString) throws JSONException {
        BufferedReader reader = null;
        try {
          //  URLConnection urlConnection = new URL(urlString).openConnection();
            //urlConnection.connect();
            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.addRequestProperty("q","chi dân");
         //   urlConnection.setDoOutput(true);
            urlConnection.connect();


            int responseCode = urlConnection.getResponseCode(); //can call this instead of con.connect()
            if (responseCode >= 400 && responseCode <= 499) {
                throw new Exception("Bad authentication status: " + responseCode); //provide a more meaningful exception message
            }
            else {

            }


            reader = new BufferedReader(new InputStreamReader(
                    urlConnection.getInputStream(),"UTF-8"));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            return new JSONArray(sb.toString());
        } catch (JSONException e) {
            throw e;
        } catch (Exception e) {
            LogHelper.e(TAG, "Failed to parse the json for media list", e);
            return null;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    private void addCollection(final Context context) {


        DatabaseReference playlist = FirebaseDatabase.getInstance().getReference().child("Collection");

        playlist.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot collection : dataSnapshot.getChildren()) {
                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();
                    String time = collection.child("time").getValue().toString();


                    RealmController realmController = new RealmController(context);
                    group.enter();
                    addCollectCon(genre, realmController);

                    realmController.add_collection(name, genre, icon, MediaIDHelper.COLLECTION_MENU, time);
                }
                group.leave();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });


    }

    private void add_thienca(final Context context) {

        DatabaseReference thienca = FirebaseDatabase.getInstance().getReference().child("ThienCa");

        thienca.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot collection : dataSnapshot.getChildren()) {
                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();

                    RealmController realmController = new RealmController(context);
                    group.enter();
                    load_thienca_item(genre, realmController);

                    realmController.add_collection(name, genre, icon, MediaIDHelper.THIEN_CA, "unknow");
                }
                group.leave();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });


    }


    private void load_KinhNhac(final Context context){


        DatabaseReference playlist = FirebaseDatabase.getInstance().getReference().child("KinhNhac");
        group.enter();

        playlist.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot collection : dataSnapshot.getChildren()) {
                    String key = collection.getKey();
                    group.enter();
                    load_Item(MediaIDHelper.KINHNHAC, key);

                }
                group.leave();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });

    }

    private void load_NgheKinh(final Context context) {


        DatabaseReference playlist = FirebaseDatabase.getInstance().getReference().child("NgheKinh");
        group.enter();

        playlist.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot collection : dataSnapshot.getChildren()) {
                    String key = collection.getKey();
                    group.enter();
                    load_Item(MediaIDHelper.NGHEKINH, key);

                }
                group.leave();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });

    }

    private void load_Item(String table_name, final String key) {

        DatabaseReference colect = FirebaseDatabase.getInstance().getReference().child(table_name).child(key).child("content");

        colect.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                for (DataSnapshot collection : dataSnapshot.getChildren()) {

                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();
                    String time = collection.child("time").getValue().toString();
                    String source = collection.child("source").getValue().toString();
                    String duration = collection.child("duration").getValue().toString();



                    realmController.add_song(genre+". "+name, icon, time, time, genre, Integer.parseInt(duration.toString()), key, key+genre, source);



                }
                group.leave();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });
    }





    private void addCollectCon(final String colectID, final RealmController realmController) {

        DatabaseReference colect = FirebaseDatabase.getInstance().getReference().child("Collection").child(colectID).child("content");

        colect.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                for (DataSnapshot collection : dataSnapshot.getChildren()) {

                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();
                    String time = collection.child("time").getValue().toString();
                    String source = collection.child("source").getValue().toString();
                    String duration = collection.child("duration").getValue().toString();



                    realmController.add_song(genre+". "+name, icon, time, time, genre, Integer.parseInt(duration.toString()), colectID, colectID+genre, source);



                }
                group.leave();



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });
    }

    private void load_thienca_item(final String colectID, final RealmController realmController) {

        DatabaseReference colect = FirebaseDatabase.getInstance().getReference().child("ThienCa").child(colectID).child("content");

        colect.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot collection : dataSnapshot.getChildren()) {

                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();
                    String time = collection.child("time").getValue().toString();
                    String source = collection.child("source").getValue().toString();
                    String duration = collection.child("duration").getValue().toString();


                    realmController.add_song(genre + ". " + name, icon, time, time, genre, Integer.parseInt(duration.toString()), colectID, colectID + genre, source);


                }
                group.leave();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });
    }

    private void load_niembut(final RealmController realmController) {


        DatabaseReference colect = FirebaseDatabase.getInstance().getReference().child("NiemBut");

        colect.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot collection : dataSnapshot.getChildren()) {

                    String genre = collection.getKey();
                    String name = collection.child("name").getValue().toString();
                    String icon = collection.child("image").getValue().toString();
                    String time = collection.child("time").getValue().toString();
                    String source = collection.child("source").getValue().toString();
                    String duration = collection.child("duration").getValue().toString();


                    realmController.add_song(genre + ". " + name, icon, time, time, genre, Integer.parseInt(duration.toString()), MediaIDHelper.NIEM_BUT, MediaIDHelper.NIEM_BUT + genre, source);


                }
                group.leave();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                group.leave();
            }
        });
    }
}
