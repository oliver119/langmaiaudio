package com.langmai.audio.model;

import android.support.v4.media.MediaMetadataCompat;

import java.util.ArrayList;

/**
 * Created by User on 8/3/2017.
 */

public class TopList {
    public static ArrayList<MediaMetadataCompat> Toplist;
    public static ArrayList<MediaMetadataCompat> Klist;
    public static ArrayList<MediaMetadataCompat> Uslist;
    public static ArrayList<MediaMetadataCompat> Playlist;

    public static ArrayList<MediaMetadataCompat> getPlaylist() {
        return Playlist;
    }

    public static void setPlaylist(ArrayList<MediaMetadataCompat> playlist) {
        Playlist = playlist;
    }

    public static ArrayList<MediaMetadataCompat> getKlist() {
        return Klist;
    }

    public static void setKlist(ArrayList<MediaMetadataCompat> klist) {
        Klist = klist;
    }

    public static ArrayList<MediaMetadataCompat> getUslist() {
        return Uslist;
    }

    public static void setUslist(ArrayList<MediaMetadataCompat> uslist) {
        Uslist = uslist;
    }

    public static ArrayList<MediaMetadataCompat> getToplist() {

        return Toplist;
    }

    public void setToplist(ArrayList<MediaMetadataCompat> toplist) {
        Toplist = toplist;
    }
}
