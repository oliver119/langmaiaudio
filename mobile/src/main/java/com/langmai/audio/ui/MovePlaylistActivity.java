package com.langmai.audio.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.langmai.audio.Adapter.PlaylistAdapter;
import com.langmai.audio.Adapter.onClickItem;
import com.langmai.audio.Dataset.Add_Dataset;
import com.langmai.audio.Enity.Playlist_Item;
import com.langmai.audio.R;
import com.langmai.audio.utils.MediaIDHelper;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 8/14/2017.
 */

public class MovePlaylistActivity extends Activity {
    private List<Playlist_Item> PlayList = new ArrayList<Playlist_Item>();
    private RecyclerView recyclerView;
    private PlaylistAdapter mAdapter;
    private KProgressHUD hud;

    private TextView Add_playlist, CancelPlaylist;
    private Add_Dataset add_dataset = new Add_Dataset();
    Bundle extras;
    String name, allbum, artist, icon, duration, source, song_id, gener;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_playlist);


        ///set popup

        DisplayMetrics dm = new DisplayMetrics();

        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int w = dm.widthPixels;
        int h = dm.heightPixels;

        getWindow().setLayout((int) (w * .6), (int) (h * .8));
        ///
        hud = KProgressHUD.create(MovePlaylistActivity.this)
                .setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100);

        hud.setProgress(90);
        //////

        recyclerView = (RecyclerView) findViewById(R.id.list_view_playlist);
        mAdapter = new PlaylistAdapter(PlayList, this, new onClickItem() {
            @Override
            public void onClick(View view, int position) {
                final Playlist_Item event = PlayList.get(position);
                AlertDialog.Builder builder = new AlertDialog.Builder(MovePlaylistActivity.this);

                builder.setTitle("Confirm");
                builder.setMessage("Do you want move " + name + " song to " + event.getPlaylist_name() + " Playlist ?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {


                        add_dataset.move_playlist(event.getPlaylist_name(), song_id, name, duration, allbum, artist, source, icon, gener);

                        dialog.dismiss();
                        setResult(MediaIDHelper.RESULT_OK);
                        Toast.makeText(MovePlaylistActivity.this, "Moved Successfully", Toast.LENGTH_SHORT).show();
                        finish();

                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();


            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        hud.show();
        loaddata();


        if (savedInstanceState == null) {
            extras = getIntent().getExtras();
            if (extras == null) {
                name = null;
                allbum = null;
                artist = null;
                duration = null;
                icon = null;
                source = null;
                song_id = null;
                gener = null;

            } else {

                name = extras.getString(MediaIDHelper.NAME_SONG);
                allbum = extras.getString(MediaIDHelper.ALLBUM_SONG);
                artist = extras.getString(MediaIDHelper.ARTIST_SONG);
                duration = extras.getString(MediaIDHelper.DURATION_SONG);
                icon = extras.getString(MediaIDHelper.ICON_SONG);
                source = extras.getString(MediaIDHelper.SOURCE_SONG);
                song_id = extras.getString(MediaIDHelper.ID_SONG);
                gener = extras.getString(MediaIDHelper.GENRE_SONG);
            }
        } else {
            name = (String) savedInstanceState.getSerializable(MediaIDHelper.NAME_SONG);
            allbum = (String) savedInstanceState.getSerializable(MediaIDHelper.ALLBUM_SONG);
            artist = (String) savedInstanceState.getSerializable(MediaIDHelper.ARTIST_SONG);
            duration = (String) savedInstanceState.getSerializable(MediaIDHelper.DURATION_SONG);
            icon = (String) savedInstanceState.getSerializable(MediaIDHelper.ICON_SONG);
            source = (String) savedInstanceState.getSerializable(MediaIDHelper.SOURCE_SONG);
            song_id = (String) savedInstanceState.getSerializable(MediaIDHelper.ID_SONG);
            gener = (String) savedInstanceState.getSerializable(MediaIDHelper.GENRE_SONG);

        }


        Add_playlist = (TextView) findViewById(R.id.add_btn);
        CancelPlaylist = (TextView) findViewById(R.id.cancel_btn);

        CancelPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Add_playlist.setVisibility(View.GONE);
    }

    private void loaddata() {
        DatabaseReference playlist = FirebaseDatabase.getInstance().getReference().child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("playlist");

        playlist.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()) {


                    if (child.getKey().equals(gener)) {
                    } else {
                        Playlist_Item event = new Playlist_Item();
                        if (child.child("image").exists() == false) {
                            event.setPlaylist_name(child.getKey());
                            PlayList.add(event);
                        } else {
                            event.setImage_url(child.child("image").getValue().toString());
                            event.setPlaylist_name(child.getKey());
                            PlayList.add(event);
                        }

                    }
                }


                hud.dismiss();

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                hud.dismiss();
            }
        });

    }


}

