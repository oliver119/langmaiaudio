package com.langmai.audio.Dataset;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by User on 8/14/2017.
 */

public class Delete_Dataset {
    DatabaseReference database = FirebaseDatabase.getInstance().getReference();

    public void deleteItem_Playlist(String playlist_name, String id_song) {
        DatabaseReference remove = database.child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("playlist").child(playlist_name).child("song").child(id_song);
        remove.removeValue();


    }

    public void delete_Playlist(String playlist_name) {
        DatabaseReference remove = database.child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("playlist").child(playlist_name);
        remove.removeValue();

    }
}
